// @flow
import React, { Component } from 'react'
import styled from 'styled-components'
import { Container,Link, Imagen, ImagenMobile, StyledLink } from './Styles/LogoStyle'


export default class Logo extends Component {
   
    render() {

        return (
            <Container>
              <StyledLink target="_blank" href="https://www.avantrip.com">
                <figure>
                  <Imagen
                    alt={'Avantrip'}
                    src={'/css/logo-avt-rojo.svg'}
                  ></Imagen>
                  <ImagenMobile
                    alt={'Avantrip'}
                    src={'/css/logo-avt-blanco.svg'}
                  ></ImagenMobile>
                </figure>
              </StyledLink>
            </Container>
        )
    }
}