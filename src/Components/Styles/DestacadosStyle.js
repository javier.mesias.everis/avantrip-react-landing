import React, { Component } from 'react'
import styled from 'styled-components'

export const Externo = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  padding: 16px;
  @media (max-width: 768px) {
    
  }
`

export const Interno = styled.div`
  margin-bottom: 24px;
  width: 21%;
  @media (max-width: 768px) {
     width: 100%;
  }
`


