import React, { Component } from 'react'
import styled from 'styled-components'

export const Toggle = styled.div`
  display: none;
  position: relative;
  padding: 5px;
  margin: 5px 0px;
  flex-direction: column;
  top: -53px;
  height: 30px;
  width: 30px;
  cursor: pointer;
  z-index: 10;
  @media (max-width: 768px) {
    display: flex;
  }
`
export const IconBarTop = styled.div`
  height: 5px;
  background: ${ props => props.theme.text ? props.theme.text : 'white' };
  margin: 3px 0px;
  transition: all 0.2s ease;
  transform: ${ props => props.open ? 'rotate(-45deg) translate(-25%, 7px)' : 'rotate(0deg) translate(0px, 0px)' };
`
export const IconBarMiddle = styled.div`
  height: 5px;
  background: ${ props => props.theme.text ? props.theme.text : 'white' };
  margin: 3px 0px;
  transition: all 0.1s ease;
  width: ${ props => props.open ? '0%' : '100%' };
`
export const IconBarBottom = styled.div`
  height: 5px;
  background: ${ props => props.theme.text ? props.theme.text : 'white' };
  margin: 3px 0px;
  transition: all 0.2s ease;
  transform: ${ props => props.open ? 'rotate(45deg) translate(-25%, -6px)' : 'rotate(0deg) translate(0px, 0px)' };
`

