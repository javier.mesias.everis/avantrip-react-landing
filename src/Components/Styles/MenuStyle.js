import React from 'react'
import styled from 'styled-components'

export const Container = styled.div`
  width: 100%;
  z-index: 10000;
  @media (max-width: 768px) {
    overflow: hidden;
    position: initial;
    height: 50px;
    transition: height 0.3s ease;
    -ms-overflow-style: none;
    overflow: -moz-scrollbars-none;
    &::-webkit-scrollbar {
      width: 0 !important;
    }
  }
`
export const Nav = styled.nav`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  background: ${ props => props.theme.primary ? props.theme.primary : '#FFFFFF' };
  color: ${ props => props.theme.text ? props.theme.text : 'white' };
  min-height: ${ props => props.theme.height ? props.theme.height + 'px' : '50px' };
  @media (max-width: 768px) {
    position: relative;
    flex-direction: column;
    background: ${ props => props.theme.primary ? props.theme.primary : '#FF0000' };
    align-items: flex-start;
    min-width: 200px;
    min-height: 100%;
  }
`

export const Header = styled.div`
  display: none;
  @media (max-width: 768px) {
    display: flex;
    flex-direction: row-reverse;
    align-items: center;
    z-index: 10;
    width: 100%;
  }
`

export const Titulo = styled.p`
  color: red;
  font-family: 'Roboto Regular', arial;
  @media (max-width: 768px) {
  		display:none;
  }
`