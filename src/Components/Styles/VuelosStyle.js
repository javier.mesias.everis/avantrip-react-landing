import React, { Component } from 'react'
import styled from 'styled-components'

export const Externo = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 10px;
  @media (max-width: 768px) {
      display:none
  }
`

export const Interno = styled.div`
  margin-right:25px;
  @media (max-width: 768px) {
    
  }
`

export const DestacadoTitulo = styled.p`
  color: #585858;
  font-size: 32px;
  font-family: 'Roboto Regular', arial;
  letter-spacing: -2px;
  padding-top: 10px;
  @media (max-width: 768px) {
    
  }
`

