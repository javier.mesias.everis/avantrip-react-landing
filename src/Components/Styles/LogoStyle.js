import React, { Component } from 'react'
import styled from 'styled-components'

export const Container = styled.div`

  @media (max-width: 768px) {
    
  }
`
export const Link = styled.a`
  display: block;
  margin: auto;
  text-decoration: none;
  @media (max-width: 768px) {

  }
`

export const Imagen = styled.img`

  @media (max-width: 768px) {
    display:none;
  }
`

export const ImagenMobile = styled.img`

  display:none;
  @media (max-width: 768px) {
     display:inline;
  }
`

export const StyledLink = styled.a`
  text-decoration:none;
`