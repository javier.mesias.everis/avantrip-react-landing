import React, { Component } from 'react'
import styled from 'styled-components'

export const Input = styled.select.attrs(({ size }) => ({
  type: "select",
  
  padding: size || "1em"
}))`
  color: #585858;
  font-size: 1em;
  font-weight: bold;
  font-family: 'Roboto Regular', arial;
  border: 1px solid #585858;
  border-radius: 3px;
  width:90%;
  -webkit-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.12);
  -moz-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.12);
  box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.12);
  display:none;
  /* here we use the dynamically computed props */
  
  padding: ${props => props.padding};

  @media (max-width: 768px) {
    display:inline
  }


`;

