import React, { Component } from 'react'
import styled from 'styled-components'

export const Container = styled.div`

	width:100%;
	height:108px;
	text-align:center;
	background: rgba(87,92,84,1);
	background: -moz-linear-gradient(left, rgba(87,92,84,1) 0%, rgba(50,120,119,1) 100%);
	background: -webkit-gradient(left top, right top, color-stop(0%, rgba(87,92,84,1)), color-stop(100%, rgba(50,120,119,1)));
	background: -webkit-linear-gradient(left, rgba(87,92,84,1) 0%, rgba(50,120,119,1) 100%);
	background: -o-linear-gradient(left, rgba(87,92,84,1) 0%, rgba(50,120,119,1) 100%);
	background: -ms-linear-gradient(left, rgba(87,92,84,1) 0%, rgba(50,120,119,1) 100%);
	background: linear-gradient(to right, rgba(87,92,84,1) 0%, rgba(50,120,119,1) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#575c54', endColorstr='#327877', GradientType=1 );
	@media (max-width: 768px) {
    	margin-top: -16px;
  	}
`

export const TituloVuelos = styled.p`

  color: #F2F2F2;
  font-size: 21px;
  padding-top: 10px;
  font-family: 'Roboto Regular', arial;
  letter-spacing: -1px;
  @media (max-width: 768px) {
   
  }
`

export const Miami = styled.span`

  color: #FFF;
  font-size: 21px;
  padding-top: 10px;
  font-weight:bold;
  font-family: 'Roboto Regular', arial;
  letter-spacing: -1px;
  @media (max-width: 768px) {
   
  }
`