import React, { Component } from 'react'
import styled from 'styled-components'

export const Caja = styled.div`
  background:#FFF;
  border: 1px solid #A4A4A4;
  border-radius: 7px 7px 7px 7px;
  -moz-border-radius: 7px 7px 7px 7px;
  -webkit-border-radius: 7px 7px 7px 7px;
  width: 100%;
  float: left;
  padding:2px;
  cursor: pointer;
  -webkit-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.12);
  -moz-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.12);
  box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.12);
  font-family: 'Roboto Regular', arial;
  @media (max-width: 768px) {
     float: none;
     width: 100%;
  }
`

export const Detalle = styled.div`
  color:#FF0000;
  font-size:18px;
  width:100%;
  text-align: left;
  margin-left: 5px;
  font-weight: bold;
  padding-bottom: 5px;

  clear:both;
  @media (max-width: 768px) {
    
  }
`

export const Imagen = styled.div`
  width:100%;
  @media (max-width: 768px) {
    
  }
`

export const Precio = styled.p`
  color:#0040FF;
  font-size:12px;
  font-weight:bold;
  @media (max-width: 768px) {
    
  }
`

export const DivContenido = styled.div`
 width:100%
  @media (max-width: 768px) {
    
  }
`

export const DivContenidoL = styled.div`
  float: left;
  margin-left:10px;
  width:50%;
  @media (max-width: 768px) {
    
  }
`

export const DivContenidoR = styled.div`
  float: right;
  margin-right:10px;
  @media (max-width: 768px) {
    
  }
`

export const Escala = styled.span`
  font-size: 12px;
  float:left;
  @media (max-width: 768px) {
    
  }
`

export const EscalaTexto = styled.span`
 margin-left:8px
  @media (max-width: 768px) {
    
  }
`
export const Costo = styled.h3`
  color:#FF0000;
  font-size:18px;
  text-align: right;
  margin-right: 5px;
  font-weight: bold;

  @media (max-width: 768px) {
    
  }
`

export const VerVuelo = styled.div`
  color:#01A9DB;
  font-size:12px;
  text-align: right;
  margin-right: 5px;
  font-weight: bold;
  clear:both;
  padding-bottom:10px;
  cursor: pointer;
  @media (max-width: 768px) {
    
  }
`

export const StyledLink = styled.a`
  text-decoration:none;
  color:#2E2E2E;
`
