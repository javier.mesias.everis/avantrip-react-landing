import React, { Component } from 'react'
import styled from 'styled-components'

export const Caja = styled.div`
  background:#FFF;
  border: 1px solid #A4A4A4;
  border-radius: 7px 7px 7px 7px;
  -moz-border-radius: 7px 7px 7px 7px;
  -webkit-border-radius: 7px 7px 7px 7px;
  width: 100%;
  float: left;
  padding:2px;
  cursor: pointer;
  -webkit-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.12);
  -moz-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.12);
  box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.12);

  @media (max-width: 768px) {
     float: none;
     width: 100%;
  }
`

export const Detalle = styled.p`
  color:#424242;
  font-size:12px;
  font-family: 'Roboto Regular', arial;
  @media (max-width: 768px) {
    
  }
`

export const Precio = styled.p`
  color:#0040FF;
  font-size:12px;
  font-weight:bold;
  font-family: 'Roboto Regular', arial;
  @media (max-width: 768px) {
    
  }
`


