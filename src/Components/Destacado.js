import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from 'styled-components'
import { Caja,Detalle, Imagen, Precio, DivContenido, DivContenidoL, DivContenidoR, Escala, EscalaTexto , Costo, VerVuelo, StyledLink } from './Styles/DestacadoStyle'


export default class Destacado extends Component {
  static propTypes = {
    id: PropTypes.string,
    name: PropTypes.string,
    bestPrice: PropTypes.string
  };

  

  render() {
    const { parentId, scales, price, description, link, img  } = this.props;

    return (
            <StyledLink  target="_blank" href={link}>
              <Caja>
                <Imagen> <img width="100%" src={img}/> </Imagen>
                <DivContenido>
                  <DivContenidoL>
                        <Escala><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" height="12px" width="12px"><path d="M17.8 21.9l1.6-1.6L17 9.4l4.6-4.6c.7-.7.7-1.8 0-2.5s-1.8-.7-2.5 0L14.6 7 3.7 4.5 2.1 6.2l8.7 4.6-4.6 4.6-2.9-.5L2 16.1l3.8 2.1L7.9 22l1.3-1.3-.5-2.9 4.6-4.6 4.5 8.7z"></path><path d="M12-5l17 17-17 17-17-17L12-5z" fill="none"></path></svg>
                        <EscalaTexto>{scales}</EscalaTexto></Escala>
                        <Detalle> {description} </Detalle>
                  </DivContenidoL>
                   <DivContenidoR>
                        <Escala>Precio desde</Escala>
                        <Costo>
                          {new Intl.NumberFormat('en', { style: 'currency', currency: 'USD' }).format(price)}
                        </Costo>
                  </DivContenidoR>
                </DivContenido>
                <VerVuelo>VER VUELO</VerVuelo>
              </Caja>
            </StyledLink>

    )
  }
}
