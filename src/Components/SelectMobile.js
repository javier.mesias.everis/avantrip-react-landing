import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from 'styled-components'
import { Input } from './Styles/SelectMobileStyle'

export default class SelectMobile extends Component {
  
  showDestacados = (e) => {
    let index = e.nativeEvent.target.selectedIndex;
    this.props.clickCaja(e.target.value, e.nativeEvent.target[index].text)
  }

  render() {
    const { opciones } = this.props;
    return (
      
         <div>
          <Input id="selectMobile" onChange={this.showDestacados} placeholder="A small text input" size="1em">
             {
                opciones.map(vuelo => {
                  return (
                      <option key={vuelo.id} value={vuelo.id} >{vuelo.name}</option>
                  )
                })
             }
          </Input>
        </div>

    )
  }
}
