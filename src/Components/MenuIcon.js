// @flow
import React, { Component } from 'react'
import styled from 'styled-components'
import { Toggle,IconBarTop, IconBarMiddle,IconBarBottom } from './Styles/MenuIconStyle'

export default class MenuIcon extends Component {
    displayName = 'Menu Icon'
    props: Props
    render() {
      return (
          <Toggle role="button">
            <IconBarTop  />
            <IconBarMiddle  />
            <IconBarBottom  />
          </Toggle>
      )
    }
}