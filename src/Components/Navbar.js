// @flow
import React, { Component } from 'react'
import styled, { ThemeProvider } from 'styled-components'
import MenuIcon from './MenuIcon'
import Logo from './Logo'
import { Container,Nav, Header, Titulo } from './Styles/MenuStyle'

export class Navbar extends Component {
  
    constructor() {
        super() 
    }
   
    render() {

        return (
            
              <Container >
                <Nav >
                  <Logo  />
                  <Header >
                    <MenuIcon  />
                  </Header>
                  <Titulo><strong>Viajar es la guita mejor invertida</strong></Titulo>
                </Nav>
              </Container>
         
        )
    }
}