import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from 'styled-components'
import { Caja,Detalle, Precio } from './Styles/VueloStyle'

export default class Movie extends Component {
  static propTypes = {
    id: PropTypes.string,
    name: PropTypes.string,
    bestPrice: PropTypes.string
  };

  showDestacados = (e) => {
    this.props.clickCaja(this.props.id, this.props.name)
    if (document.getElementById("selectMobile"))
        document.getElementById("selectMobile").value  = this.props.id
  }

  render() {
    const { id, name, bestPrice } = this.props;

    return (
      
            <Caja className="pp" onClick={this.showDestacados} >
              <Detalle> {name} </Detalle>
              <Precio> Desde <span></span>
               {new Intl.NumberFormat('en', { style: 'currency', currency: 'USD' }).format(bestPrice)}
              </Precio>
            </Caja>

    )
  }
}
