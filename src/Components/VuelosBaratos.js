import React, { Component } from "react";
import styled from 'styled-components'
import { Container,TituloVuelos, Miami } from './Styles/VuelosBaratosStyle'
import Vuelos from './Vuelos'


export  class VuelosBaratos extends Component {
 
    render() {
      return (
        <Container>
          	<TituloVuelos>
          		Los vuelos mas baratos para tu estadia en <Miami>Miami</Miami>
          	</TituloVuelos>
          	<Vuelos />
          
        </Container>
      );
    }
}
