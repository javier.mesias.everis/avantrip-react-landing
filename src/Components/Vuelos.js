import React, { Component } from "react";
import styled from 'styled-components'
import PropTypes from "prop-types";
import Vuelo from './Vuelo';
import SelectMobile from './SelectMobile';
import Destacados from './Destacados';
import { Externo,Interno, DestacadoTitulo } from './Styles/VuelosStyle'

export default class Vuelos extends Component {
  
  state = { vuelos: [] , caja:0 , titulo: 'Todas las estadías' }

  constructor () {
  	super()
    fetch('https://api.myjson.com/bins/95cj2')
      .then(res => res.json())
      .then(results => {
        this.setState({ vuelos: results })
      })
  }

  _clickCaja = (numero,titulo) => {
    this.setState({ caja:numero, titulo:titulo })
  }

  _renderResults () {
    return <div> 
            <DestacadoTitulo> Vuelos destacados en <strong> {this.state.titulo} </strong>  </DestacadoTitulo>  
            <Destacados caja={this.state.caja} /> 
           </div>
  }

  render() {
  	const { vuelos = [] } = this.state;
    return (
      <div>
        <SelectMobile
          clickCaja={this._clickCaja} 
          opciones={vuelos} 
          caja={this.state.caja} 
        />
        <Externo >
          {
            vuelos.map(vuelo => {
              return (
                <Interno key={vuelo.id} >
                  <Vuelo 
                    clickCaja={this._clickCaja}
                    id={vuelo.id}
                    name={vuelo.name}
                    bestPrice={vuelo.bestPrice}
                  />
                </Interno>
              )
            })
          }
        </Externo>
        {this._renderResults()}
      </div>
    )
  }
}
