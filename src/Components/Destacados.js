import React, { Component } from "react";
import styled from 'styled-components'
import PropTypes from "prop-types";
import Destacado from './Destacado';
import { Externo,Interno } from './Styles/DestacadosStyle'

export default class Destacados extends Component {
  
  state = { destacados: [] }

  constructor (props) {
  	super(props)
    fetch('https://api.myjson.com/bins/j6qk6')
      .then(res => res.json())
      .then(results => {
        this.setState({ destacados: results })
      })
  }

  _procesaResultado (destacados) {
    if (this.props.caja == 0 || destacados.length == 0 )
        return destacados;

    let destacados_aux = [];
    destacados.map((detacado, index) => {
          if (detacado.parentId == this.props.caja ){
              destacados_aux.push(detacado);
          }
    })

    return destacados_aux;
  }

  irALink = (link) => {
      console.log(link);
  }

  render() {
  	const { destacados = [] } = this.state;
    let data = this._procesaResultado(destacados);
    
    return (
      <Externo >
        {
          data.map((detacado, index) => {
            return (
              <Interno key={index} >
                <Destacado 
                    parentId={detacado.parentId}
                    scales={detacado.scales}
                    price={detacado.price}
                    description={detacado.description}
                    link={detacado.link}
                    img={detacado.img}
                  />
              </Interno>
            )
          })
        }
      </Externo>
    )
  }
}
