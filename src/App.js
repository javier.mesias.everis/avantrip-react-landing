import React, { Component } from 'react';
import { Navbar } from './Components/Navbar'
import { VuelosBaratos } from './Components/VuelosBaratos'

class App extends Component {
  render() {
    return (
       <div>
	       <Navbar/>
	       <VuelosBaratos/>
       </div>
    );
  }
}

export default App;
