## La finalidad de la página es brindarle al usuario una landing específica de vuelos para su destino con los mejores precios.

# Pasos para instalar en un ambiente local

## Instrucciones

- Clonar el repositorio

```
git clone https://gitlab.com/javier.mesias.everis/avantrip-react-landing.git
```

- Entrar al directorio avantrip-React-Landing y ejecutar el comando 

```
npm install
```

- Dentro del directorio avantrip-React-Landing ejecutar el comando npm start esto levantara la app en http://localhost:3000/

```
npm start
``` 

Para ver el build generado online ingresar a [javier-mesias.surge.sh](http://javier-mesias.surge.sh/).

## Fin instalación local
